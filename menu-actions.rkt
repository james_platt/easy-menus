#lang racket

(require racket/gui
         framework
         handy/utils)

(provide (all-defined-out))

; Actions for a File menu, Open item for a frame with a default text editor in it.
; Note that finder:get-file is cross-platform despite having the same name as the
; Macintosh file browser.
(define/contract (dte-file-open-actions 
                  file-to-open ;The path to the file as a text%.  This may or may not actually be displayed in the GUI.
                  #:file-contents-container [file-contents-container #f];Also a text% but this has the contents of the file, as displayed in the GUI.
                  )(->*
                    ((is-a?/c text%))
                    (#:file-contents-container (or/c (is-a?/c text%) #f))
                    thunk?
                    )
  (thunk
   (say "entered dte-file-open-actions")
   (define file-open-path (finder:get-file))
   (define file-open-text (path->string file-open-path))
   (send file-to-open erase);Erase the default text.
   (send file-to-open insert file-open-text)
   (when file-contents-container
     (send file-contents-container erase)
     (send file-contents-container insert (with-input-from-file file-open-path (thunk (port->string))))
     )
   )
  )

;Actions for File menu, Save with default text editor.
(define/contract (dte-file-save-actions
                  file-to-open ;The path to the file as a text%.  This may or may not actually be displayed in the GUI.
                  working-file-text ;Also a text% but this has the contents of the file, as displayed in the GUI.
                  )(->*
                    ((is-a?/c text%)
                     (is-a?/c text%))
                    thunk?
                    )
  (thunk
  (with-output-to-file (string->path (send file-to-open get-text))
    (thunk (display (send working-file-text get-text)))
    #:exists 'replace)
  )
)


;Actions for File menu, Save As with default text editor.
(define (dte-file-save-as-actions
                  file-to-open ;The path to the file as a text%.  This may or may not actually be displayed in the GUI.
                  working-file-text ;Also a text% but this has the contents of the file, as displayed in the GUI.
                  )(->*
                    ((is-a?/c text%)
                     (is-a?/c text%))
                    thunk?)
  (thunk
  (say "entered file-save-as")
  (define file-open-path (finder:put-file))
  (send file-to-open erase);Erase the existing destination file.
  (send file-to-open insert (path->string file-open-path))
  (with-output-to-file file-open-path
    (thunk (display (send working-file-text get-text)))
    #:exists 'replace)
  )
 )
